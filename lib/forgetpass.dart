import 'package:flutter/material.dart';


void main() => runApp(forget());

class forget extends StatefulWidget {
  @override
  _forgetState createState() => _forgetState();
}

    TextEditingController Emailfp = TextEditingController();
    TextEditingController Telfp = TextEditingController();

    final _formKey = GlobalKey<FormState>();

class _forgetState extends State<forget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Forget Pass',
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            color: Colors.blueGrey,
          ),
          Padding(
            padding: const EdgeInsets.all(35.0),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: Text(
                      'กรุณากรอกข้อมูลที่ลงทะเบียนไว้',
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    validator: (String value) {
                      if (value.isEmpty) return 'กรอก email';
                    },
                    controller: Emailfp,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                        prefixIcon: Icon(Icons.email),
                        labelText: 'EMAIL',
                        labelStyle: TextStyle(
                            fontSize: 20.0
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0)
                        )
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    validator: (String value) {
                      if (value.isEmpty) return 'กรอกเบอร์โทร';
                    },
                    controller: Telfp,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                        prefixIcon: Icon(Icons.phone_iphone),
                        labelText: 'TEL',
                        labelStyle: TextStyle(
                            fontSize: 20.0
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0)
                        )
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  RaisedButton(onPressed: () => forgetpass(),
                    color: Colors.white,
                    child: Text('Confirm',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  forgetpass() {
    if(_formKey.currentState.validate()){
      String email = Emailfp.text;
      String tel = Telfp.text;
    }
  }
}
