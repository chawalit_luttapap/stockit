import 'package:flutter/material.dart';
import 'package:stockit/forgetpass.dart';
import 'package:stockit/group.dart';
import 'package:stockit/register.dart';



class Loginpage extends StatefulWidget {
  @override
  _LoginpageState createState() => _LoginpageState();
}

      TextEditingController Username = TextEditingController();
      TextEditingController Password = TextEditingController();

      final _formKey = GlobalKey<FormState>();

class _LoginpageState extends State<Loginpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            color: Colors.blueGrey,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Welcome To Stock IT',
                    style: TextStyle(
                      fontSize: 30.0,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    validator: (String value) {
                      if (value.isEmpty) return 'กรอก username';
                    },
                    controller: Username,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                        prefixIcon: Icon(Icons.supervised_user_circle),
                        labelText: 'USERNAME',
                        labelStyle: TextStyle(
                            fontSize: 20.0
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(20.0,10.0,20.0,10.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                        )
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    validator: (String value) {
                      if (value.isEmpty) return 'กรอก password';
                    },
                    obscureText: true,
                    controller: Password,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                        prefixIcon: Icon(Icons.vpn_key),
                        labelText: 'PASSWORD',
                        labelStyle: TextStyle(
                            fontSize: 20.0
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                        ),
                    ),
                  ),
                  SizedBox(
                      height: 20.0
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => group()));
                      },
                        color: Colors.white,
                        child: Text('LOGIN',
                          style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),

                      RaisedButton(onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => register()));
                      },
                        color: Colors.white,
                        child: Text('REGISTER',
                          style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ],
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => forget()));
                    },
                    child: Text(
                      'Forget Password',
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: 20.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  //loginApp() {
    //if (_formKey.currentState.validate()) {
      //String username = Username.text;
      //String password = Password.text;
    //}
  //}
}
