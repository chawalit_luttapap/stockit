import 'package:flutter/material.dart';
import 'package:stockit/login_page.dart';

void main() => runApp(Login());

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
     theme: ThemeData(
       primaryColor: Colors.pink
     ),
      title: 'Login',
      home: Loginpage() ,
    );
  }
}


