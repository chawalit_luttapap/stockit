import 'package:flutter/material.dart';
import 'package:stockit/addpro.dart';

void main()=> runApp(prod());

class prod extends StatelessWidget {

  Icon search = Icon(Icons.search);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Center(
          child: Text(''
              'Product'
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: SizedBox(
              width: 70.0,
                child: Icon(Icons.search)),
            onPressed: (){},
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[

        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: (){
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => addpro()));
          },
          label: Text('ADD'),
        icon: Icon(Icons.add),
        backgroundColor: Colors.teal,
      ),
    );
  }
}

