import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stockit/forgetpass.dart';
import 'package:stockit/group.dart';


class register extends StatefulWidget {
  @override
  _registerState createState() => _registerState();
}

      TextEditingController Usernamere = TextEditingController();
      TextEditingController Passwordre = TextEditingController();
      TextEditingController Emailre = TextEditingController();
      TextEditingController Telre = TextEditingController();

      final _formKey = GlobalKey<FormState>();


class _registerState extends State<register> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Register',
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            color: Colors.blueGrey,
          ),
          Padding(
            padding: const EdgeInsets.all(35.0),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: Text(
                      'กรุณากรอกข้อมูล',
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    validator: (String value) {
                      if (value.isEmpty) return 'กรอก username';
                    },
                    controller: Usernamere,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                        prefixIcon: Icon(Icons.supervised_user_circle),
                        labelText: 'USERNAME',
                        labelStyle: TextStyle(
                            fontSize: 20.0
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0)
                        )
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    validator: (String value) {
                      if (value.isEmpty) return 'กรอก password';
                    },
                    controller: Passwordre,
                    obscureText: true,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                        prefixIcon: Icon(Icons.vpn_key),
                        labelText: 'PASSWORD',
                        labelStyle: TextStyle(
                            fontSize: 20.0
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0)
                        )
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    validator: (String value) {
                      if (value.isEmpty) return 'กรอก email';
                    },
                    controller: Emailre,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                        prefixIcon: Icon(Icons.email),
                        labelText: 'EMAIL',
                        labelStyle: TextStyle(
                            fontSize: 20.0
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0)
                        )
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    validator: (String value) {
                      if (value.isEmpty) return 'กรอกเบอร์โทร';
                    },
                    controller: Telre,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        errorStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                        prefixIcon: Icon(Icons.phone_iphone),
                        labelText: 'TEL',
                        labelStyle: TextStyle(
                            fontSize: 20.0
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0)
                        )
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  RaisedButton(onPressed: () {},
                    color: Colors.white,
                    child: Text('Confirm',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  forgetpass() {
    if(_formKey.currentState.validate()){
      String username = Usernamere.text;
      String password = Passwordre.text;
      String email = Emailre.text;
      String tel = Telre.text;
    }
  }
}
