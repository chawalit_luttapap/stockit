import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:stockit/prod.dart';


import 'namenamename.dart';

void main() => runApp(group());

class group extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Center(
          child: Text(
            'Storage',
          ),
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            color: Colors.white,
          ),
          Padding(
            padding: const EdgeInsets.all(35.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: Text(
                    'My Group',
                    style: TextStyle(
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0)
                  ),
                  child: ListTile(
                    leading: Icon(Icons.unarchive,size: 50,),
                    title: Text(
                        'GroupA'
                    ),
                    onTap: (){
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => prod()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(
          size: 20.0
        ),
        visible: true,
        closeManually: false,
        curve: Curves.bounceIn,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        onOpen: ()=>print('open dial'),
        onClose: ()=>print('dial close'),
        backgroundColor: Colors.teal,
        foregroundColor: Colors.black,
        elevation: 8.0,
        shape: CircleBorder(),
        children: [
          SpeedDialChild(
            child: Icon(Icons.add),
            backgroundColor: Colors.pinkAccent,
            label: 'Add Group',
            labelStyle: TextStyle(
              fontSize: 18.0,
            ),
            onTap: () {
              Alert(
                context: context,
                title: 'Add Group',
                content: Column(
                  children: <Widget>[
                    TextField(
                      decoration: InputDecoration(
                        icon: Icon(Icons.shop_two),
                        labelText: '+NameGroup',
                      ),
                    ),
                  ],
                ),
                buttons: [
                  DialogButton(
                    onPressed: (){},
                    child: Text(
                      'OK',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                      ),
                    ),
                  )
                ],
              ).show();
            }
          ),
          SpeedDialChild(
            child: Icon(Icons.person),
            backgroundColor: Colors.blue,
            label: 'Profile',
            labelStyle: TextStyle(
              fontSize: 18.0,
            ),
            onTap: (){
              Navigator.push(context,
              MaterialPageRoute(builder: (context) => name()));
            },
          ),
        ],
      ),
    );
  }
}
